// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import './assets/default.scss'
import * as VMaps from 'vue2-google-maps'

Vue.use(VMaps, {
	load: {
		key: 'AIzaSyC8aIoJDlSgmsLkSvWD8ldmWyESCa575-g',
		libraries: 'places',
	}
})

Vue.config.productionTip = true

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	template: '<App/>',
	components: { App }
})
